#start by opening the file that was saved using GNS3
tracefile = open("/Users/pouyankeshavarzian/Desktop/573Project1_CSV.txt", "r")

#Declare counter variables for each direction of packet flow
server_to_client = 0
client_to_server = 0

#Start a loop that goes through the opened file in line by line
for line in tracefile:
#Sort By lines containing UDP
  if "UDP" in line:
#Increment counters as applicable depending on traffic flow
    if "\"192.168.0.1\",\"192.168.0.2\"" in line:
      server_to_client = server_to_client + 1
    if "\"192.168.0.2\",\"192.168.0.1\"" in line:
      client_to_server = client_to_server + 1

#print the output of the counters
print "The number of packets from 192.168.0.1 to 192.168.0.2 is ", server_to_client
print "The number of packets from 192.168.0.2 to 192.168.0.1 is ", client_to_server
